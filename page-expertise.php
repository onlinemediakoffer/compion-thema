<?php
/**
 * Template Name: Expertise
 *
 * @package compion
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<section class="container section_type_intro" >
				<div class="container__innersize__small column">
					<div class="intro column__left" data-aos="fade-right" data-aos-once="true">
						<?php if( get_field('field_subtitle') ): ?>
				              <span class="sub-title"><?php the_field('field_subtitle'); ?></span>
						<?php endif; ?>
						<h1>Focus op kwaliteit en effectiviteit</h1>
						<p>We ontzorgen jou graag op het gebied van communicatie. Hierbij werken we samen met jou én je doelgroep. Ons doel is namelijk om hen te bereiken. Daarom vinden wij de wens van je doelgroep zelfs belangrijker dan die van jou of onszelf!</p>
						<p>
						Onze werkwijze? Wij beginnen bij voorkeur altijd via een krachtig <strong>concept en strategie</strong>, waarna we over gaan naar het <strong>realiseren</strong> van alles dat nodig is om deze strategie uit te voeren. Daarna duiken we de data in om resultaten te meten. Door te testen, te evalueren en te <strong>optimaliseren</strong> werken we continu aan een hogere conversie. Als jouw doelgroep verandert of groeit, groeien wij mee. En zo begint onze infinity-cirkel opnieuw. Een aanpak die uit ervaring steevast leidt tot hogere kwaliteit én effectiviteit.</P>
					</div>
					<div class="intro column__right">
						<img class="verticalImage" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Lemniscaat4.png"/>
					</div>
				</div>
			</section>


			<section class="container section_type_expertise">
				<div class="container__innersize__wide column concept" data-aos="fade-left" data-aos-once="true">
					<div class="column__left">
						
					</div>
					<div class="column__right">
						<h3>Concept &amp; strategie</h3>
						<p>Welk doel en welke doelgroep wil je bereiken? Dit brengen we samen in kaart, waarna we een strategie uitstippelen én een krachtig verhaal maken dat jouw merk gaat vertellen. Laat je verrassen!</p>
						<ul>
							<li><a href="<?php echo get_site_url(); ?>/tag/communicatiestrategie/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Communicatiestrategie</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/marketingstrategie/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Marketingstrategie</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/contentstrategie/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Contentstrategie</a></li>
							<li>Campagneconcepten</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/positioneringsadvies/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Positioneringsadvies</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/customer-journey/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Customer Journey</a></li>
						</ul>
					</div>
				</div>
			
				<div class="container__innersize__wide column realisatie" data-aos="fade-right" data-aos-once="true">
					<div class="column__left">
						<h3>Realisatie</h3>
						<p>Wij realiseren alle onderdelen die nodig zijn om ons advies tot leven te brengen: van huisstijl tot maatwerk-websites en van campagnes tot het verzorgen van je drukwerk. Wij kunnen (bijna) álles!</p>
						<ul>
							<li><a href="<?php echo get_site_url(); ?>/tag/logo-huisstijl/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Logo & Huisstijl</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/website-webshop/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Website / webshop</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/printdesign/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Printdesign</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/digital-design/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Digital design</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/buitenreclame/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Buitenreclame</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/advertenties/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Advertenties</a></li>
							<li>Narrow casting</li>
							<li><a href="<?php echo get_site_url(); ?>/tag/tekst/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Tekst</a></li>
							<li>Foto, video & animatie</li>
						</ul>
					</div>
					<div class="column__right">
					</div>
				</div>
				<div class="container__innersize__wide column optimalisatie" data-aos="fade-left" data-aos-once="true">
					<div class="column__left">
					</div>
					<div class="column__right">
						<h3>Optimalisatie</h3>
						<p>We blijven kritisch, vooral op ons eigen werk. Daarom schaven en schuren we continu aan gerealiseerde onderdelen in een traject. Resultaten meten we, conversies verbeteren we. Waar nodig gaan we terug naar het concept. Altijd met je doelgroep en doel in het vizier.</p>
						<ul>
							<li><a href="<?php echo get_site_url(); ?>/tag/website-optimalisatie/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Website optimalisatie</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/data-analyse/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Data analyse</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/seo/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">SEO - Zoekmachine optimalisatie</a></li>
							<li><a href="<?php echo get_site_url(); ?>/tag/sea/" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">SEA - Online advertenties</a></li>
							<li>Expert review</li>
						</ul>
					</div>
				</div>
				<?php get_template_part( 'template-parts/content-block', 'cta' );
				?>
			</section>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
