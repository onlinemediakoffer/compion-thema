<?php
/**
 * Template Name: Contact
 *
 * @package compion
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="container section_type_intro">
				<div class="container__innersize__small column">
					<div class="intro column__twoThird">
						<h3 class="subTitle">Contact</h3>
						<h1>Ons kantoor staat voor je open!</h1>
						<p>Wil je ons eens ontmoeten, een keertje koffiedrinken, brainstormen of samenwerken? Er zijn vele wegen naar Compion, dus laat het ons weten! </P>
						<a href="mailto:info@compion.nl" target="_blank" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">info@compion.nl</a>
						<a href="tel:+31582038076" target="_blank" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">+31(0)58 - 203 80 76</a>
					</div>
				</div>
			</section>

			<section class="container section_type_contactCrystalic" data-aos="fade-left" data-aos-once="true">
				<div class="container__innersize__wide column">
					<div class="column__left"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Crystalic.png"></div>
					<div class="column__right">
						<h3>Kom je op bezoek?</h3>
						<b>Crystalic Business Park<br>
							Kantoor 0.22<br>
							François HaverSchmidtwei 2<br>
							8914 BC Leeuwarden</b>

						<p>We zitten in Crystalic, het iconische glazen gebouw vlakbij afslag Leeuwarden-Noord. Als je door de hoofdingang komt, sla je linksaf. Vervolgens kom je ons kantoor aan de rechterkant tegen. Een bel hebben we niet, dus loop gerust binnen!</p>
						<a href="https://goo.gl/maps/i7EdvbkbTGUc55uh7" target="_blank" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Plan route</a>
					</div>
				</div>
			</section>

			<section class="container section_type_contactOther" data-aos="fade-left" data-aos-once="true">
				<div class="container__innersize__small column">
					<div class="column__left">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/contact-1.jpg">
						<h3>Meer weten over ons?</h3>
						<p>Wie zijn die mensen achter Compion eigenlijk? Wat kun je van ons verwachten? Onze unieke werkwijze al gelezen en benieuwd naar meer? Kom gezellig langs en ontdek wie we zijn!</p>
						<a href="<?php echo get_site_url(); ?>/over-ons"  class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Ontdek ons</a></div>
					<div class="column__right">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/contact-3.jpg">
						<h3>Teamlid bereiken</h3>
						<p>Misschien heb je een specifieke vraag voor een van ons of wil je graag even sparren. Gooi het er gerust uit en mail of bel! Wij staan altijd voor je klaar.</p>
						<a href="<?php echo get_site_url(); ?>/over-ons" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">Ons team</a>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
