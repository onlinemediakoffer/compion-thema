<?php
/**
 * Template Name: Ons werk
 *
 * @package compion
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="container section_type_intro">
				<?php
					while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				</section>
				<!-- <section class="container section_type_clients">
					<div class="container__innersize__wide clients with-padding">
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/provfriesland.png" alt="Provincie Fryslan" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/nissan.png" alt="Nissan" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/nhl.png" alt="NHL Hogeschool" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/infiniti.png" alt="Infinti" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/innofest.png" alt="Innofest" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/fbto.png" alt="FBTO" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/paradigm.png" alt="Paradigm" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/alfacollege.png" alt="Alfa College" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/aegon.png" alt="Aegon" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/rijkswaterstaat.png" alt="Rijkswaterstaat" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/rug.png" alt="RUG" />
						</div>
						<div class="client">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/provfriesland.png" alt="Provincie Fryslan" />
						</div>
						
				    </div>
				</section> -->
				<section class="container section_type_cases">
					<!-- <div class="container__innersize__small">
						<h2>Uitgelicht werk</h2>
						<p class="intro">Er zijn interessante cases, spannende cases, bijzondere cases en gewoon hele mooie cases. En wij vinden ze allemaal minstens even leuk! Daarom geven we ze met gepaste trots een eigen plek op onze site.</p>
					</div> -->
					<div class="container__innersize__wide">
						<?php
								$the_query = new WP_Query(array(
				            'category_name' => 'cases',
				            'post_status' => 'publish',
				        ));
				        ?>
				        <div class="contentGrid">
					        <?php if ($the_query->have_posts()) : ?>
					            <?php while ($the_query->have_posts()) : $the_query->the_post(); 
					                get_template_part( 'template-parts/content', get_post_type() );

					            endwhile; ?>
					            <?php wp_reset_postdata(); ?>

					        <?php else : ?>
					            <p><?php __('We hebben op dit moment geen cases online staan.'); ?></p>
					        <?php endif; ?>
					    </div>
				    </div>
				</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
