<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="css/fontawesome/all.css" rel="stylesheet">
</head>
<body class="stylesheet">
	<div class="containerStyle">
		<h1 class="pagetitle"><strong>Styleblad</strong> van Compion</h1>
		<div class="blockItem">
		<h3 class="blockTitle">Kleuren</h3>
			<ul class="colorList">
				<li><div class="colorBlock red"></div>$Red
				</li>
				<li><div class="colorBlock green"></div>$green
				</li>
				<li><div class="colorBlock golden"></div>$golden
				</li>
				<li><div class="colorBlock black"></div>$black
				</li>
				<li><div class="colorBlock white"></div>$white
				</li>
				<li><div class="colorBlock offwhite"></div>$offwhite
				</li>
			</ul>

		</div>
		<div class="blockItem">
		<h3 class="blockTitle">Typografie</h3>
			<h1>H1 title Sofia Pro Black 2.2rem</h1>
			<h2>H2 title Sofia Pro Bold 1.8rem</h2>
			<h3>H3 title Sofia Pro Bold 1.5rem</h3>
			<h4>H4 title Sofia Pro regular 1.1rem</h4>
			<h5>H5 title Sofia Pro Bold 0.9rem</h5>
			<br/><br/>
			<strong>Strong - Sofia Pro Bold</strong>
			<br/><br/>
			<p>Alinea - Sofia Pro Light - 300 - 1.1rem<br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada lacus sed purus luctus tempus. Suspendisse tristique diam vitae laoreet tempor. Mauris vel auctor ligula, in hendrerit libero. Nunc sit amet risus mauris. Donec efficitur turpis ac tempor tincidunt. Ut tincidunt sem id risus sagittis, a ultricies diam vehicula. Nullam posuere fermentum lacus. In eleifend venenatis consequat. Mauris ligula odio, <strong>rhoncus id lobortis in</strong>, ultrices nec massa. In hac habitasse platea dictumst. Vivamus nec justo eros. Sed sit amet neque non felis dictum dictum.<br/><br/>
			<i> Nulla eu felis sed leo aliquet facilisis sed non purus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</i></p>
			<br/><br/>
		</div>
		<div class="blockItem">
			<h3 class="blockTitle">Labels</h3>
			<ul class="normalList">
				<li><label class="tags red">Categorie naam</label><span>.tags .red</span></li>
				<li><label class="tags green">Tag naam</label><span>.tags .green</span></li>
			</ul>
		</div>
		<div class="blockItem">
			<h3 class="blockTitle">Buttons</h3>
			<ul class="normalList">
				<li>
					<a class="btn btn_type_rounded btn_color_red btn_iconposition_right btn_icon_arrowright">Button rounded</a><span>.btn_type_rounded</span><span>.btn_color_red</span><span>.btn_iconposition_right</span><span>.btn_icon_arrowright</span>
				</li>
				<li>
					<a class="btn btn_type_rounded btn_color_green btn_iconposition_left btn_icon_arrowleft">Button rounded</a><span>.btn_type_rounded</span><span>.btn_color_green</span><span>.btn_iconposition_left</span><span>.btn_icon_arrowleft</span>
				</li>
				<li>
					<a class="btn btn_type_rounded btn_color_golden btn_iconposition_downright btn_icon_arrowdown">Button rounded</a><span>.btn_type_rounded</span><span>.btn_color_golden</span><span>.btn_iconposition_downright</span><span>.btn_icon_arrowdown</span>
				</li>
				<li>
					<a class="btn btn_type_rounded btn_color_blue btn_iconposition_downleft btn_icon_arrowdown">Button rounded</a><span>.btn_type_rounded</span><span>.btn_color_blue</span><span>.btn_iconposition_downleft</span><span>.btn_icon_arrowdown</span>
				</li>
			</ul>
			<ul class="normalList">
				<li>
					<a class="btn btn_type_text btn_color_red btn_iconposition_right btn_icon_arrowright">Button text</a>
					<span>.btn_type_text</span><span>.btn_color_red</span><span>.btn_iconposition_right</span><span>.btn_icon_arrowright</span>
				</li>
				<li>
					<a class="btn btn_type_text btn_color_black btn_iconposition_right btn_icon_arrowright">Button text</a>
					<span>.btn_type_text</span><span>.btn_color_red</span><span>.btn_iconposition_right</span><span>.btn_icon_arrowright</span>
				</li>
				<li>
					<a class="btn btn_type_text btn_color_green btn_iconposition_left btn_icon_arrowleft">Button text</a>
					<span>.btn_type_text</span><span>.btn_color_green</span><span>.btn_iconposition_left</span><span>.btn_icon_arrowleft</span>
				</li>
				<li>
					<a class="btn btn_type_text btn_color_golden btn_iconposition_downright btn_icon_arrowdown">Button text</a>
					<span>.btn_type_text</span><span>.btn_color_golden</span><span>.btn_iconposition_downright</span><span>.btn_icon_arrowdown</span>
				</li>
				<li>
					<a class="btn btn_type_text btn_color_blue btn_iconposition_downleft btn_icon_arrowdown">Button text</a>
					<span>.btn_type_text</span><span>.btn_color_blue</span><span>.btn_iconposition_downleft</span><span>.btn_icon_arrowdown</span>
				</li>
			</ul>
		</div>
		<div class="blockItem">
			<h3 class="blockTitle">Gradients - full</h3>
			<ul class="gradientList">
				<li>
					<div class="gradientBlock"><div class="gradientOverlay gradient_color_blue-green-full"></div></div><span>.gradient_color_blue-green-full</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientOverlay gradient_color_yellow-green-full"></div></div><span>.gradient_color_yellow-green-full</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientOverlay gradient_color_red-blue-full"></div></div><span>.gradient_color_red-blue-full</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientOverlay gradient_color_red-green-full"></div></div><span>.gradient_color_red-green-full</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientOverlay gradient_color_yellow-red-full"></div></div><span>.gradient_color_yellow-red-full</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientOverlay gradient_color_yellow-blue-full"></div></div><span>.gradient_color_yellow-blue-full</span>
				</li>
			</ul>
			<h3 class="blockTitle">Gradients - Semi</h3>
			<ul class="gradientList">
				<li>
					<div class="gradientBlock"><div class="gradientPhotoBg"></div><div class="gradientOverlay gradient_color_blue-green-semi"></div></div><span>.gradient_color_blue-green-fsemi</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientPhotoBg"></div><div class="gradientOverlay gradient_color_yellow-green-semi"></div></div><span>.gradient_color_yellow-green-semi</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientPhotoBg"></div><div class="gradientOverlay gradient_color_red-blue-semi"></div></div><span>.gradient_color_red-blue-semi</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientPhotoBg"></div><div class="gradientOverlay gradient_color_red-green-semi"></div></div><span>.gradient_color_red-green-semi</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientPhotoBg"></div><div class="gradientOverlay gradient_color_yellow-red-semi"></div></div><span>.gradient_color_yellow-red-semi</span>
				</li>
				<li>
					<div class="gradientBlock"><div class="gradientPhotoBg"></div><div class="gradientOverlay gradient_color_yellow-blue-semi"></div></div><span>.gradient_color_yellow-blue-semi</span>
				</li>
			</ul>
		</div>
	</div>
</body>