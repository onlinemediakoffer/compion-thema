<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

get_header();

$term 				= get_queried_object();
$afwijkendeTitle 	= get_field('afwijkende_titel_voor_tag-pagina', $term);
$productBlok_true 	= get_field('productblok_gebruiken', $term);
$productTitle 		= get_field('titel_van_het_productblok', $term);
$productDescription = get_field('productbeschrijving', $term);

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="container__innersize__small">	
					<header class="page-header">
						<?php 
						if( $afwijkendeTitle){ ?>
							<h1><?php the_field('afwijkende_titel_voor_tag-pagina', $term); ?></h1>
						<?php }
						
						else{ 
							the_archive_title( '<h1 class="page-title">Blogs en cases over ', '</h1>' );
						}?>

						<?php
						if ( $productBlok_true ==1){
						?>		
							<div class="column tagContentBlock">
								<div class="column__left">
									<?php if ( tag_description() ) : // Show an optional tag description ?>
									<div class="archive-meta"><?php echo tag_description(); ?></div>
									<?php endif; ?>
								</div>
								<div class="column__right productBlock">
									<h3><?php echo $productTitle; ?></h3>
									<p><?php echo $productDescription; ?></p>
									<?php
										// check if the repeater field has rows of data
										if( have_rows('product_usp', $term) ):?>
											<ul class="tag-product-usp">
										    <?php while ( have_rows('product_usp', $term) ) : the_row();?>
										        <li>
										        	<?php the_sub_field('product_usp', $term); ?>
										    	</li><?php
										    endwhile;
										    ?></ul><?php
										else :
										    // no rows found
										endif;
										?>

									<?php get_template_part( 'template-parts/content-block', 'cta' );?>
								</div>
							</div>
						<?php }
						else{ ?>
							<div class="tag-content">
								<?php if ( tag_description() ) : // Show an optional tag description ?>
								<div class="archive-meta"><?php echo tag_description(); ?></div>
								<?php endif; ?>

								<?php get_template_part( 'template-parts/content-block', 'cta' );?>
							</div>
						<?php } 
						if( $afwijkendeTitle){
							the_archive_title( '<h3>Blogs en cases over ', '</h3>' );
						}
						else{ 

						}
						?>
					</header><!-- .page-header -->
				</div>
			</div>
			<div class="container">
				<div class="container__innersize__wide">
					<?php
						$tag = get_query_var('tag'); 
						echo do_shortcode('[ajax_load_more posts_per_page="8"  scroll="false" button_label="Toon meer berichten" button_loading_label="Bezig met laden" tag="'.$tag.'"]');
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();