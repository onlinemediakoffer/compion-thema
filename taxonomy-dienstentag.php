<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="container__innersize__small">	
					<header class="page-header">
						<?php
						the_archive_title( '<h1 class="page-title">Alles over ', '</h1>' );
						?>
						<?php if ( tag_description() ) : // Show an optional tag description ?>
						<div class="archive-meta"><?php echo tag_description(); ?></div>
					<?php endif; ?>
						
					</header><!-- .page-header -->
				</div>
			</div>
			<div class="container">
				<div class="container__innersize__wide">
					<?php
						$tag = get_query_var('tag'); 
						echo do_shortcode('[ajax_load_more posts_per_page="8"  scroll="false" button_label="Toon meer berichten" button_loading_label="Bezig met laden" tag="'.$tag.'"]');
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();