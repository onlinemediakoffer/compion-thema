<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package compion
 */

get_header(); ?>

		<div class="blogContainer">

					<div id="primary" class="content-area">
						<main id="main" class="site-main" role="main">

								
							<!-- <h1 class="entry-title"><?php the_title(); ?></h1> -->
							
							<?php
						while ( have_posts() ) : the_post();
							
							get_template_part( 'template-parts/content', 'blog' );

							//the_post_navigation();

						endwhile; // End of the loop.
						?>


						</main><!-- #main -->
					</div><!-- #primary -->



					<aside id="secondary" class="widget-area" role="complementary">
						<?php dynamic_sidebar( 'blog-sidebar' ); ?>
					</aside><!-- #secondary -->
		</div>


<?php
//get_sidebar();
get_footer();
