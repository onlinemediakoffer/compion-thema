<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package compion
 */

?>


	</div><!-- #content -->

	
</div><!-- #page -->
<footer>
	<div class="container">
		<div class="container__innersize__wide footerDetails">
			<div>
				<img class="footerLogo" src="<?= get_stylesheet_directory_uri() .'/images/logo-compion.svg'; ?>" title="Compion logo">
				<br/>
				<p>Jouw communicatiepartner</p>
			</div>
			<div>
				<h5>Kom eens op bezoek</h5>
				<p>
					<a href="https://www.google.com/maps/place/Compion/@53.205542,5.7627913,17z/data=!3m1!4b1!4m5!3m4!1s0x47c8fe841e0403df:0xdd32a4f25f45b591!8m2!3d53.205542!4d5.76498"  target="_blank" title="Bezoek ons kantoor">Crystalic Business Park<br/>
					Kantoor 0.22<br/>
					François HaverSchmidtwei 2<br/>
					8914 BC Leeuwarden
					</a>
				</p>
			</div>
			<div>
				<h5>Bel of mail gerust</h5>
				<p>
					<a href="tel:031582038076‬" target="_blank">058-2038076‬</a><br>
					<a href="mailto:info@compion.nl" target="_blank">info@compion.nl</a>
				</p>
			</div>
			<div>
				<h5>Blijf verbonden met ons</h5>
				<p>
					<a href="http://eepurl.com/gt4vw5" target="_blank" title="Nieuwsbrief Compion">Compion Courant (nieuwsbrief)</a><br>
					<a href="https://www.linkedin.com/company/1768941/admin/" target="_blank" title="LinkedkIn pagina Compion">LinkedIn</a><br>
					<a href="https://www.facebook.com/compion.nl/" target="_blank" title="Facebookpagina Compion">Facebook</a><br>
					<a href="https://www.instagram.com/compion.nl" target="_blank" title="Instagram pagina Compion">Instagram</a><br>
				</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="container__innersize__wide copyright">
			<div>© Compion | Alle rechten gereserveerd</div>
			<div><?php wp_nav_menu( array(
				'theme_location' => 'menu-3',
				'menu_id'        => 'copyright-menu',
				'container'	     =>	 'div',
					) );
				?>		
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>

</footer>

<script>
function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}
</script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
	   AOS.init({
	  disable: 'mobile'
	});
  </script>
</body>
</html>
