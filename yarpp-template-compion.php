<?php 
/*
YARPP Template: Compion
Author: mitcho (Michael Yoshitaka Erlewine)
Description: A simple example YARPP template.
*/
?>
<div class="container__innersize__small">
<?php get_template_part( 'template-parts/content-block', 'cta' );
				?>
			</div>
<div class="container__innersize__wide yarpp-container">

	<?php if (have_posts()):?>
		
	<h3>Dit vind je misschien ook interessant</h3>
	<ol class="contentGrid">
		<?php while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-aos="fade-left" data-aos-once="true" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
	<a href="<?php echo esc_url( get_permalink() ); ?>">
			<header class="entry-header">
			<div class="overlay">
			</div>
		</header>
		<div class="entry-content">
		<?php
			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
					foreach((get_the_category()) as $category) { 
						echo '<label class="tags red">' .$category->cat_name . '</label>'; } 
					?>
					<?php 
						$terms = wp_get_post_terms($post->ID, 'dienstentag');
						$count = count($terms);
						if ( $count > 0 ) {
						    foreach ( $terms as $term ) {
						        echo '<label class="tags green">' .$term->name . '</label>';;
						    }
						}
					?>
				</div>
			<?php endif; 
				the_title( '<h3 class="entry-title">', '</h3>' );
			?>
			<div class="fakebutton"></div>
		</div><!-- .entry-content -->	




		</a>

		</article><!-- #post-<?php the_ID(); ?> --><!-- (<?php the_score(); ?>)-->
		<?php endwhile; ?>
	</ol>
	<?php else: ?>
	<?php endif; ?>
</div>
