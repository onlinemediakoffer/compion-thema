<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */


?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container__innersize__wide">

		<header class="entry-header">
			<div class="thumbnail" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');"></div>

			<div class="entry-meta-top container__innersize__small">
				 <!--CATEGORIE-->
				 <?php
					$categories = get_the_category();
					$separator = ' ';
					$output = '';
					if($categories){
					    foreach($categories as $category) {
					        $output .= '<a class="tags red" href="'.get_category_link( $category ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
					    }
					echo trim($output, $separator);
					}
				?>
				<time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time>
			</div><!-- .entry-meta -->

			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<span class="archive-content-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></span>' );
			endif;

			if ( 'post' === get_post_type() ) :
				?>
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'compion' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'compion' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->
		
	</div>
	<div class="entry-meta container__innersize__wide">
		<div class="entry-meta-inner container__innersize__small">
			<!--DIENSTEN TAGS-->
			Lees content met dezelfde tags:<br/>
			<?php 
				$terms = wp_get_post_terms($post->ID, 'dienstentag');
				$count = count($terms);
				if ( $count > 0 ) {
				    foreach ( $terms as $term ) {
				        echo '<a class="tags green" href="' .get_term_link( $term->slug, 'dienstentag') .'">' . $term->name . '</a>';
				    }
				}
			?>

			<!--NORMALE TAGS-->
			<?php
				global $post;
				foreach(get_the_tags($post->ID) as $tag)
				{
				    echo '<a class="tags green" href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
				}
			?>
		</div>
	</div>


</article><!-- #post-<?php the_ID(); ?> -->

