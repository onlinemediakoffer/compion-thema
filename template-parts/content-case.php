<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="container__innersize__wide column">
			<div class="column__left case__image">
				<?php compion_post_thumbnail(); ?>
			</div>
			<div class="column__right">
				<?php					
					$image = get_field('logo_opdrachtgever');
					if( !empty($image) ): ?>
						<img  class="case__logo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; 
				the_title( '<h1 class="entry-title">', '</h1>' );
				the_excerpt();?>
			</div>
		</div>
		<?php if( get_field('payoff_van_de_case') ): ?>
              <h2 class="case__payoff"><?php the_field('payoff_van_de_case'); ?></h2>
		<?php endif; ?>

		
	</header><!-- .entry-header -->
	<div class="container__innersize__wide">
		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->
	</div>
	
	<div class="entry-meta container__innersize__wide">
		<div class="entry-meta-inner container__innersize__small">
			<!--DIENSTEN TAGS-->
			Lees content met dezelfde tags:<br/>
			<!-- <?php 
				$terms = wp_get_post_terms($post->ID, 'dienstentag');
				$count = count($terms);
				if ( $count > 0 ) {
				    foreach ( $terms as $term ) {
				        echo '<a class="tags green" href="' .get_term_link( $term->slug, 'dienstentag') .'">' . $term->name . '</a>';
				    }
				}
			?> -->

			<!--NORMALE TAGS-->
			<?php
				global $post;
				foreach(get_the_tags($post->ID) as $tag)
				{
				    echo '<a class="tags green" href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
				}
			?>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

