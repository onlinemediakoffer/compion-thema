<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

?>

<nav class="main-navigation">
	<?php
		wp_nav_menu( array(
			'theme_location' => 'menu-1',
			'menu_id'        => 'primary-menu',
			'container'	     =>	 'false',
		) );
	?>
</nav><!-- .main-navigation -->


<span class="mobile-toggle" onclick="openNav()">Menu <i class="fas fa-bars"></i></span>