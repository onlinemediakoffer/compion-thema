<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

?>

<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'compion' ); ?></a>


<div class="siteHeaderHome">
	<?php get_template_part( 'template-parts/header', 'branding' ); ?>
	<div class="siteHeader__left">
		<div class="omk-logo">Voorheen Online Media Koffer</div>
	</div>
	<div class="siteHeader__right">
		<?php get_template_part( 'template-parts/header', 'menu' ); ?>
		<div class="text" data-aos="fade-left" data-aos-once="true">
			<h1><span class="sub-title">Wij zijn jouw</span>
			communicatiepartner</h1>
			<p>Betrokken communicatiebureau in Leeuwarden, met een sterke focus op branding, strategie en creatie. Wij werken graag samen. Jij en wij, als Compions. </p><br/>
			<a href="<?php echo get_site_url(); ?>/expertise" title="Over ons" class="btn btn_type_rounded btn_color_red btn_iconposition_right btn_icon_arrowright">Onze expertise</a>
			<a href="<?php echo get_site_url(); ?>/over-ons" class="btn btn_type_text btn_color_red btn_iconposition_right btn_icon_arrowright">Leer ons kennen</a>
		</div>
	</div>
</div>