<?php
/**
 * Template part for displaying header on contactpage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

?>

<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'compion' ); ?></a>

<header class="siteHeader">
	<div class="siteHeader__left">
		<?php get_template_part( 'template-parts/header', 'branding' ); ?>
	</div>
	<div class="siteHeader__right">
		<?php get_template_part( 'template-parts/header', 'menu' ); ?>
	</div>

</header><!-- #masthead -->
<div class="bg_type_fullgradient">
	<div class="overlay">
	</div>
</div>