<?php

/**
 * CTA Block Template.
 *
 * @param   array $block The block data including all properties and settings.
 * @param   bool $is_preview True when editing in the back-end.
 * @param   int $post_id The post being edited.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'cta-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cta';

$term = get_queried_object();
// Load values and assing defaults.
$cta_true = get_field('call_to_action_gebruiken', $term);
$title = get_field('title', $term) ?: 'Klaar voor de volgende stap in jouw communicatie? ';
$text = get_field('text', $term) ?: 'Geert helpt je graag verder!';
$person = get_field('person', $term) ?: 'Geert';

if($person == 'Geert'){
    $phone = '+31618044356';
    $mail = 'geert@compion.nl';
    $photo = '/images/team/compion-geert-close.jpg';
}
else if($person == 'Karinus'){
    $phone = '+31614935866';
    $mail = 'karinus@compion.nl';
    $photo = '/images/team/compion-karinus-close.jpg';
}
else if($person == 'Joost'){
    $phone = '+31649227942';
    $mail = 'joost@compion.nl';
    $photo = '/images/team/compion-joost-close.jpg';
}
else{
    $phone = '+31618044356';
    $mail = 'geert@compion.nl';
}

if ( $cta_true ==1){
?>
    <div id="<?php echo esc_attr($id); ?>" class="column cta <?php echo $person; ?>">
        <div class="cta__block">
          <div class="cta__left"><div class="cta__image" style="background: url('<?php echo get_stylesheet_directory_uri(); ?><?php echo $photo; ?>');background-size: cover;background-position: center center;"></div>
          </div>
          <div class="cta__right">
            <h3><?php echo $title; ?></h3>
            <span><?php echo $text; ?><br/></span>
            <a href="mailto:<?php echo $mail; ?>" class="btn btn_type_rounded btn_color_blue btn_iconposition_right btn_icon_arrowright">Mail <?php echo $person; ?></a>
            <a href="tel:<?php echo $phone; ?>" class="btn btn_type_text btn_color_blue btn_iconposition_right btn_icon_arrowright">of bel <?php echo $person; ?></a>
          </div>
        </div>
    </div>
<?php
}
