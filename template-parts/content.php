<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-aos="fade-left" data-aos-once="true" >
<a href="<?php echo esc_url( get_permalink() ); ?>">
		<header class="entry-header">
			<div class="blog-bg-image" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');"></div>
		</header>
		<div class="entry-content">
		<?php
			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
					foreach((get_the_category()) as $category) { 
						echo '<label class="tags red">' .$category->cat_name . '</label>'; } 
					?>
					<?php 
						$terms = wp_get_post_terms($post->ID, 'dienstentag');
						$count = count($terms);
						if ( $count > 0 ) {
						    foreach ( $terms as $term ) {
						        echo '<label class="tags green">' .$term->name . '</label>';;
						    }
						}
					?>
				</div>
			<?php endif; 
				the_title( '<span class="archive-content-title">', '</span>' );
			?>
			<div class="fakebutton"></div>
		</div><!-- .entry-content -->
	</a>
</article><!-- #post-<?php the_ID(); ?> -->
