<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package compion
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container__innersize__small">
				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title">Oeps, hier ging iets mis!</h1>
						<p style="max-width:600px;">De pagina die je probeert te bezoeken bestaat niet (meer) of hebben we nog niet goed gelinkt. We krijgen hiervan een seintje en lossen dit zo snel mogelijk op.</p>
						<br/>
						<a href="<?php echo get_site_url(); ?>" class="btn btn_type_rounded btn_color_red btn_iconposition_right btn_icon_arrowright">Bezoek de homepage</a>
					</header><!-- .page-header -->
					<br/><br/><br/>
				</section><!-- .error-404 -->
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
