<?php
/**
 * Template Name: Over ons
 *
 * @package compion
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="container section_type_intro" data-aos="fade-left" data-aos-once="true">
				<?php
					while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				</section>
				<section class="container section_type_manifest">
					<div class="container__innersize__small column">
							<div class="column__left">
							<div class="manifest_type_text">
								<h3>Jij krijgt altijd <span class="red">kwaliteit</span></h3>
								<p>Wij streven áltijd naar de beste kwaliteit voor jouw doelgroep. We gaan voor minimaal een 9 (en stiekem voor een 10)! Met al meer dan tien jaar ervaring op het gebied van branding en communicatie, weten we precies wat we doen.</p>
							</div>
							<div class="manifest_type_text">
								<h3>Wij vinden er <span class="blue">altijd wat van</span></h3>
								<p>Jij vraagt, wij denken mee en dan pas draaien we. Met je mee, óf er tegenin. Wij zijn nuchter en eigenzinnig, zoals het noorderlingen betaamt. Wij zijn kritisch op onszelf, het vak en op onze samenwerkingen. Zo creëren we een eerlijke en duurzame relatie, waarin we blijven ontwikkelen en verbeteren.</p>
							</div>
							<div class="manifest_type_text">
								<h3>Je hoort <span class="green">van ons</span></h3>
								<p>Als Compion houden we onze ogen en oren voor je open, ook als je er niet om vraagt. Valt ons iets op? Je hoort het direct! Hebben we een leuk idee? Wij hangen aan de lijn! Zo houden we jouw communicatie optimaal én actueel.</p>
							</div>
						</div>
						<div class="column__right">
							<div class="manifest_type_text">
								<h3>Liefde voor <span class="green">het vak</span></h3>
								<p>Van uitdagende communicatiedoelen tot last-minute realisaties: elke opdracht geeft ons energie. Iets dat we graag combineren met de gezelligheid van foute muziek, goede koffie en uitstekend gezelschap. Geniet met ons mee, de deur van onze koepel staat open!</p>
							</div>
							<div class="manifest_type_text">
								<h3>Blik op de <span class="golden">toekomst</span></h3>
								<p>Wij gaan voor trouwe samenwerkingen en toekomstgerichte oplossingen. Met jouw visie in het vizier, zetten we vandaag nog de eerste stap. Tijdens de weg vooruit houden we onze ecologische voetafdruk zo klein mogelijk. We gaan voor een duurzame relatie, met jou en met Moeder Aarde.</p>
							</div>
							<div class="manifest_type_text">
								<h3>Wij zijn <span class="red">betrokken</span></h3>
								<p>Wij kennen jou, je organisatie én je doelen. En jij kent ons, daar zorgen wij voor. We houden van persoonlijk contact en successen vieren we samen. Zo ondersteunen en ontzorgen we je op de manier die bij jou past. Jij. Wij. Compions.</p>
							</div>
							<div style="clear: both;"></div>
						</div>
					</div>
				</section>
				<section class="container teamSection">
					<div class="container__innersize__small">
					<h2><span class="small">Ons team</span> Dit zijn jouw partners</h2>
					<p class="p_type_sectionintro">Dit zijn wij: een groep enthousiaste professionals, met heel veel liefde voor het vak. Een gelukkig team is één van onze eerste prioriteiten (als het niet de allereerste is). Dit heeft natuurlijk ook zijn weerslag op jou, als opdrachtgever. Want een enthousiast en betrokken team betekent nóg betere resultaten! </p>
					
					<div class="team">
						<div class="team__member gk">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_blue-green-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Specialist in online communicatie. Met zijn nuchtere helicopterview heeft Geert de touwtjes van ieder communicatieproces stevig in handen. 
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Geert Kuurstra</span>
								<span class="underTitle">Mede-eigenaar, adviseur</span>
								<a class="team__member__info__contact textBtn dark" href="tel:+31618044356" title="Bel Geert"><span>06-18044356</span></a>
								<a class="team__member__info__contact textBtn dark" href="mailto:geert@compion.nl"><span>geert@compion.nl</span></a>
								
							</div>
						</div>
						<div class="team__member kr">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_red-blue-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Onze expert op het gebied van corporate branding. Ontwikkelt overtuigende creatieve concepten en creëert krachtige visuele identiteiten.
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Karinus Rauwerda</span>
								<span class="underTitle">Mede-eigenaar, art-director</span>
								<a class="team__member__info__contact textBtn dark" href="tel:+‭31614935866" title="Bel Karinus"><span>06-14935866</span></a>
								<a class="team__member__info__contact textBtn dark" href="mailto:karinus@compion.nl"><span>karinus@compion.nl</span></a>
							</div>
						</div>
						<div class="team__member wdg">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_blue-green-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Wilmer is altijd opzoek naar de beste manier om jouw bedrijf in de markt te zetten. Hij praat in conversies en eet data als lunch.
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Wilmer de Groot</span>
								<span class="underTitle">Online marketeer, Googlelaar</span>
								<a class="team__member__info__contact textBtn dark" href="mailto:wilmer@compion.nl"><span>wilmer@compion.nl</span></a>
							</div>
						</div>
						<div class="team__member htk">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_yellow-red-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Heleen kun je alles vragen over papier en drukwerk. Grote kans dat je van deze allround designer antwoord krijgt met een visueel kunstwerk!
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Heleen ten Kate</span>
								<span class="underTitle">Designer, Papierspecialist</span>
								<a class="team__member__info__contact textBtn dark" href="mailto:heleen@compion.nl"><span>heleen@compion.nl</span></a>
							</div>
						</div>
<!-- 						<div class="team__member psst alien">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_yellow-red-semi">
								</div>
							</div>
							<div class="team__member__info">
								<h3>Ook onderdeel van dit team worden?</h3>
								<span class="underTitle">Op zoek naar een leuke stage? We hebben momenteel drie vacatures open staan.</span>
								<a class="btn" href="<?php echo site_url(); ?>/wij/vacatures">..</a>
							</div>
						</div> -->
						<div class="team__member ef">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_yellow-blue-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Erik is onze creatief schrijver. Met een doel(-groep) van een tekst in zijn vizier, zorgt hij ervoor dat elke zin als een zonnetje wegleest!
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Erik Feitsma</span>
								<span class="underTitle">Copywriter, Verhalenbekokstover</span>
								<a class="team__member__info__contact textBtn dark" href="mailto:erik@compion.nl"><span>erik@compion.nl</span></a>
							</div>
						</div>
						<div class="team__member ft">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_yellow-red-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Als tekstexpert schrijft Fenna aan de lopende band onze kanalen vol. Haar ambitie? Alles (ja, echt alles) leren op communicatiegebied! 
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Fenna Terpstra</span>
								<span class="underTitle">Stagiaire communicatie, infovreter</span>
								<a class="team__member__info__contact textBtn dark" href="mailto:fenna@compion.nl"><span>fenna@compion.nl</span></a>
							</div>
						</div>
						<div class="team__member aw">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_yellow-blue-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Flyers, posters… Wat je communicatiemiddel ook is – Anne maakt er een grafisch juweeltje van. De titel creatieve duizendpoot is niet voor niets!
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Anne Wortman</span>
								<span class="underTitle">Stagiaire grafisch vormgever, creatieve duizendpoot</span>
								<a class="team__member__info__contact textBtn dark" href="mailto:anne@compion.nl"><span>anne@compion.nl</span></a>
							</div>
						</div>
						<div class="team__member jv">
							<div class="team__member__photo">
								<div class="photostate__default"></div>
								<div class="photostate__hover"></div>
								<div class="team__member__photo__overlay gradient_color_yellow-blue-semi">
								</div>
								<div class="team__member__photo__quote">
									<span>
									Judith doet onderzoek naar de perfecte contentstrategie voor Compion. Prima taak voor deze  zelfbenoemd expert in neuromarketing!
									</span>
								</div>
							</div>
							<div class="team__member__info">
								<span class="teamTitle">Judith Veldman</span>
								<span class="underTitle">Content- en mediastrateeg, Onderzoeksjunkie</span>
								<a class="team__member__info__contact textBtn dark" href="mailto:judith@compion.nl"><span>judith@compion.nl</span></a>
							</div>
						</div>
					</div>
					<center>
						<a href="<?php echo get_site_url(); ?>/nieuws/stagevacature-copywriter/" class="btn btn_type_rounded btn_color_red btn_iconposition_right btn_icon_arrowright">Ontdek onze vacature</a>
						<br/><br/>
					</center>
				</section>
				<section class="container section_insta">
					<div class="container__innersize__small">
						<h2>Meer van onze sfeer proeven?</h2>
						<p>Via Instagram geven we geregeld een kijkje achter de schermen. Zo leer je ons alvast veilig vanaf een afstandje kennen. Bevalt het? Volg dan ons kanaal of nog beter: loop eens binnen!</p>
					</div>
					<div class="container__innersize__wide">
						<?php echo do_shortcode('[instagram-feed]');?>
					</div>
				</section>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
