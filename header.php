<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package compion
 */

?>

<?php $hero = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_enqueue_script('jquery'); ?>
	<?php wp_head(); ?>
	<!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/fontawesome/all.css" rel="stylesheet">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
	<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MQXR2D8');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
	<div class="ticker"><span>Breaking news: </span>Compion neemt BrandDogs over <a href="https://compion.nl/nieuws/compion-neemt-branddogs-over/">Lees meer <i class="fa fa-chevron-right"></i></a>
	</div>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQXR2D8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div id="myNav" class="overlay">
	  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><spann>&times;</span></a>
		  <div class="overlay-content">
		    	<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'container'	     =>	 'false',
					) );
				?>
		  </div>
	</div>
<div id="page" class="site">
	<?php if (is_front_page()) :  
		get_template_part( 'template-parts/header', 'home' );
	elseif (is_page( 'contact' )) :  
		get_template_part( 'template-parts/header', 'default' );
	else: 
		get_template_part( 'template-parts/header', 'default' );
	endif; ?>
	<div id="content" class="site-content clear">
