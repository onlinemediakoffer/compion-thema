<?php
/**
 * Template Name: Textpage
 *
 * @package compion
 */

get_header(); ?>

	<div id="primary" class="content-area container__innersize__wide">
		<main id="main" class="site-main" role="main">
			<section class="container blogContainer  section_type_intro" data-aos="fade-left" data-aos-once="true">
				<?php
					while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'textpage' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				</section>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
