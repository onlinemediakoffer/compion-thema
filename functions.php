<?php
/**
 * compion functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package compion
 */

if ( ! function_exists( 'compion_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function compion_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on compion, use a find and replace
		 * to change 'compion' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'compion', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'compion' ),
			'menu-3' => esc_html__( 'copyright', 'compion' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'compion_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'compion_setup' );

/**
 * ACF Blocks
 */
function register_acf_block_types() {

    // register a testimonial block.
    acf_register_block_type(array(
        'name'              => 'testimonial',
        'title'             => __('Testimonial'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/testimonial/testimonial.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'testimonial', 'quote' ),
    ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function compion_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'compion_content_width', 640 );
}
add_action( 'after_setup_theme', 'compion_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function compion_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'compion' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'compion' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'compion_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function compion_scripts() {
	wp_enqueue_style( 'compion-style', get_stylesheet_uri() );

	wp_enqueue_script( 'compion-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'compion-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'modernizer', get_stylesheet_directory_uri() . '/js/modernizr-custom.js', array( 'jquery' ) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'compion_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 * Remove archive title prefixes.
 *
 * @param  string  $title  The archive title from get_the_archive_title();
 * @return string          The cleaned title.
 */
function grd_custom_archive_title( $title ) {
	// Remove any HTML, words, digits, and spaces before the title.
	return preg_replace( '#^[\w\d\s]+:\s*#', '', strip_tags( $title ) );
}
add_filter( 'get_the_archive_title', 'grd_custom_archive_title' );



/**
 *
 * Color palette Gutenberg
 *
 */
// Adds support for editor color palette.
function mytheme_setup_theme_supported_features() {
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'Compion Black', 'themeLangDomain' ),
            'slug' => 'compion-black',
            'color' => '#2C2C2B',
        ),
        array(
            'name' => __( 'Compion Offwhite', 'themeLangDomain' ),
            'slug' => 'compion-offwhite',
            'color' => '#F6F1E9',
        ),
        array(
            'name' => __( 'Compion White', 'themeLangDomain' ),
            'slug' => 'compion-white',
            'color' => '#FFFFFF',
        ),
        array(
            'name' => __( 'Compion Red', 'themeLangDomain' ),
            'slug' => 'compion-red',
            'color' => '#E43260',
        ),
        array(
            'name' => __( 'Compion Golden', 'themeLangDomain' ),
            'slug' => 'compion-golden',
            'color' => '#FABB5A',
        ),
        array(
            'name' => __( 'Compion Green', 'themeLangDomain' ),
            'slug' => 'compion-green',
            'color' => '#008A80',
        ),
        array(
            'name' => __( 'Compion Blue', 'themeLangDomain' ),
            'slug' => 'compion-blue',
            'color' => '#00B4FF',
        ),
    ) );
}

add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );

/**
 * Disable the custom color picker.
 */
function tabor_gutenberg_disable_custom_colors() {
	add_theme_support( 'disable-custom-colors' );
}
add_action( 'after_setup_theme', 'tabor_gutenberg_disable_custom_colors' );


// Remove p tags from category description
remove_filter('term_description','wpautop');

// Remove compression on images
add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );


function custom_content_filter_the_content( $content ) {
	if ( in_category('cases') ) {
      $content .= get_template_part( 'template-parts/content-block', 'cta' );
  }
    return $content;
}
add_filter( 'the_content', 'custom_content_filter_the_content', 30 );

// Remove block to use html in category and tag descriptions
remove_filter('pre_term_description', 'wp_filter_kses');
