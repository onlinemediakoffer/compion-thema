<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

get_header();
?>
<div class="bow"></div>

<section class="container section_type_homeAbout">
	<div class="container__innersize__wide column">
		<div class="column__left"><span class="homeQuote">Jij vraagt, wij denken mee en dan pas draaien we.</span></div>
		<div class="column__right">
			<span class="sub-title">Zitten we helemaal met jou op één lijn? </span>
			<h2>Wij worden de beste Compions</h2>
			<p>Zie ons als jouw externe communicatieafdeling, met gedreven professionals die fluitend naar het werk gaan. Het creatieve bloed stroomt door onze aderen. De behoefte van de doelgroep zit altijd in ons hoofd. Dit leidt tot unieke en doeltreffende communicatieoplossingen. </p>
			<p>Wij gaan voor trouwe partnerschappen, waarin we elkaar écht kennen. Durf jij het aan?</p>
			<a href="<?php echo get_site_url(); ?>/over-ons" class="btn btn_type_text btn_color_red btn_iconposition_right btn_icon_arrowright">Meer over onze waarden</a>
		</div>
	</div>
</section>
<section class="container section_clientLogo">
	<div class="container__innersize__wide clientlogoBlock">
		<?php
		$post_objects = get_field('klantlogos');
		
		if( $post_objects ): ?>
		    <?php foreach( $post_objects as $post): ?>
		        <?php setup_postdata($post); ?>
	        	 <?php
		        // vars
				$caselinkurl = get_field('selecteer_de_case');
				$image = get_field('kies_het_logo');
				$size = 'medium';
		    	$thumb = $image['sizes'][ $size ];
		    	$caselink = get_field('linkt_het_logo_naar_een_case');
				
		    	if ( $caselink ==1){
				?>
		            <a class="clientlogoBlock__item" href="<?php echo $caselinkurl; ?>">
						<img src="<?php echo esc_url($thumb); ?>" />
						<div class="clientlogoBlock__item__link"><i class="fa fa-chevron-right"></i></div>
					</a>
				<?php
				}
				if ( $caselink ==0){ ?>
					<div class="clientlogoBlock__item">
						<img src="<?php echo esc_url($thumb); ?>" />
					</div>
				<?php }?>
		    <?php endforeach; ?>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
	</div>
</section>
<section class="container section_type_homeCases">
	<div class="container__innersize__wide column">
		<div class="column__left">
			<div class="intro">
				<span class="sub-title">Ons werk spreekt voor zich. </span>
				<h2>Stuk voor stuk projecten waar we trots op zijn.</h2>
				<p>Wij houden van variatie. Dat houdt ons scherp, creatief en vrolijk. Dit zie je ook terug in onze opdrachtgevers en onze cases.</p>
			</div>
			<a href="<?php echo get_site_url(); ?>/cases/compionschap-met-vos-opmaat-consultancy/" class="card case" data-aos="fade-in" data-aos-once="true">
				<div class="case-bg-image-container">
					<div class="case-image case-vos-opmaat"></div>
				</div>
				<div class="text">
					<h3>Compionschap met Vos Opmaat Consultancy</h3>
					<div class="fakebutton"></div>
				</div>
			</a>

			<div class="quote" data-aos="fade-in" data-aos-once="true">
				<h4>“De samenwerking met Compion hebben wij vanaf het eerste moment als zeer prettig ervaren. De communicatie was open, eerlijk, persoonlijk en helder.”</h4>
				<span>Jos Heusinkveld - manager verkoop fakkel.nl</span>
			</div>

			<a href="/cases/nieuwe-webshop-de-fakkel/" class="card case" data-aos="fade-in" data-aos-once="true">
				<div class="case-bg-image-container">
					<div class="case-image case-de-fakkel"></div>
				</div>
				<div class="text">
					<h3>Nieuwe webshop De Fakkel</h3>
					<div class="fakebutton"></div>
				</div>
			</a>
		</div>
		<div class="column__right">
			<a href="<?php echo get_site_url(); ?>/cases/vernieuwde-app-voor-nissan/" class="card case" data-aos="fade-in" data-aos-once="true">
				<div class="case-bg-image-container">
					<div class="case-image case-nissan"></div>
				</div>
				<div class="text">
					<h3>Vernieuwde app voor Nissan</h3>
					<div class="fakebutton"></div>
				</div>
			</a>
			<a href="<?php echo get_site_url(); ?>/cases/brain-fuel-rebranding" class="card case" data-aos="fade-in" data-aos-once="true">
				<div class="case-bg-image-container">
					<div class="case-image case-brain-fuel"></div>
				</div>
				<div class="text">
					<h3>Positioneringsadvies en rebranding voor Brain Fuel</h3>
					<div class="fakebutton"></div>
				</div>
			</a>
			
			<a href="<?php echo get_site_url(); ?>/ons-werk" title="Over ons" class="btn btn_type_text btn_color_red btn_iconposition_right btn_icon_arrowright" data-aos="fade-left" data-aos-once="true">Bekijk meer werk</a><br/><br/>
			<a href="<?php echo get_site_url(); ?>/expertise" class="btn btn_type_text btn_color_red btn_iconposition_right btn_icon_arrowright" data-aos="fade-left" data-aos-once="true">Bekijk onze diensten</a>
		</div>
	</div>
</section>
<section class="cta-section">
	<?php get_template_part( 'template-parts/content-block', 'cta' ); ?>
</section>
<?php
get_footer();
