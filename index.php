<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package compion
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="container section_type_intro">
				<div class="container__innersize__small column">
					<div class="intro column__twoThird">
						<h3 class="subTitle">Leesvoer</h3>
						<h1>Kennis, nieuws en cases</h1>
						<p>Dit is dé plek om onze kennis op te doen en op de hoogte te blijven van ons allerlaatste nieuws.</p>
					</div>
				</div>
			</section>

			<div class="container__innersize__wide section_blog">
				<?php
				if ( have_posts() ) : ?>

					<?php echo do_shortcode('[ajax_load_more posts_per_page="6" button_label="Toon meer berichten" scroll="false" button_loading_label="Bezig met laden"]');

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
